/****************/
/*****SETUP******/
/****************/
#define MS 1000                             // we use delayMicroseconds to avoid sleeping the MSP432 so we define a constant for readable conversion from milli to micro

#define PS2_DAT 14                          // P1.7 <-> brown wire
#define PS2_CMD 15                          // P1.6 <-> orange wire
#define PS2_SEL 34                          // P2.3 <-> yellow wire (also called attention)
#define PS2_CLK 35                          // P6.7 <-> blue wire

#define REMOTE  0                           // the infrared remote state is 0 (defining our states for type of control)
#define PS2     1                           // the PS2 controller state is 1
#define AUTO    2                           // the automatic movement state is 2
int STATE = REMOTE;                         // sets the starting state to be infrared remote controlled

#define IR_UP      0x46                     // forward (defining our infrared remote buttons in an intuitive way)
#define IR_DOWN    0x15                     // backward
#define IR_LEFT    0x44                     // spin left
#define IR_RIGHT   0x43                     // spin right
#define IR_OK      0x40                     // stop
#define IR_ONE     0x16                     // turn left
#define IR_THREE   0xD                      // turn right
#define IR_EIGHT   0x1C                     // change remote state to auto
#define IR_STAR    0x42                     // open Gripper
#define IR_ZERO    0x52                     // toggle remote state to PS2
#define IR_HASH    0x4A                     // close gripper
#define IR_FIVE    0x18                     // change state to Remote

#define RIGHT 100                           // we use 100 for right turns/spins
#define LEFT  101                           // we use 101 for left turns/spins

/* LIBRARY IMPORT STATEMENTS */
#include "SimpleRSLK.h"                     // import SimpleRSLK library for robot
#include "Servo.h"                          // import Servo library for gripper
#include "Romi_Motor_Power.h"               // import Romi_Motor_Power library for movement
#include "TinyIR.h"                         // import infrared sensor library
#include "PS2X_lib.h"                       // import PS2 library
#include "SR04.h"                           // import sonar library

/* GRIPPER VARIABLES */
const int closedGripperAngle = 140;         // write this value to gripper to close it
const int openGripperAngle = 0;             // write this vallue to gripper to open it
const int gripperChange = 20;               // change the gripper angle in increments/decrements of 30 degrees
int gripperAngle = 0;                       // tracks gripper's angle

/* SPEED VARIABLES */
const int fullSpeed = 100;                  // full movement speed of robot
const int halfSpeed = 50;                   // full movement speed of robot

/* DELAY VARIABLES */
const int tenthSec = 100;                   // delay of 0.1 seconds
const int halfSec = 500;                    // delay of 0.5 seconds
const int oneSec = 1000;                    // delay of 1 second
const int IRdelay = 100;                    // sets the delay for the IR sensor

/* PIN DEFINITIONS */
const int IRpin = 33;                       // pin 5.1 on the board
const int rightTrigEcho = 2;                // pin 6.0 on the board
const int leftTrigEcho = 44;                // pin 8.2 on the board

Romi_Motor_Power rightWheel;                // create rightWheel object from Romi_Motor_Power class
Romi_Motor_Power leftWheel;                 // create leftWheel object from Romi_Motor_Power class

/* CREACTION OF OBJECTS */
SR04 leftSonar = SR04(leftTrigEcho);        // initializes the left sonar sensor
SR04 rightSonar = SR04(rightTrigEcho);      // initializes the right sonar sensor
Servo gripper;                              // create gripper object from Servo class
IRData IRresults;                           // global variable to track inputs from infrared remote
PS2X ps2x;                                  // create PS2 Controller Class

/* PS2 CONFIGURATION */
#define pressures false                     // don't need to take into account button pressure
#define rumble false                        // don't need rumble

/* PS2 VARIABLES */
const int restingCutoff = 5;                // accounts for calibration errors for reading joystick position
const int stopCutoff = 100;                 // defines how far the right joystick needs to be pressed downwards to stop the robot's movement
const int minJoystick = 0;                  // minimum raw value from PS2 joysticks
const int maxJoystick = 255;                // maximum raw value from PS2 joysticks
const int stopConversion = 127;             // maximum mapped value for stopping
const int spinConversion = 127;             // maximum speed for spinning
const int forwardBackwardConversion = 150;  // maximum speed for forward/backward movement
const int turnConversion = 100;             // maximum speed for turning

/* LINE SENSOR VARIABLES */
uint16_t sensorVal[LS_NUM_SENSORS];         // creates an array of the values from the line sensors
uint16_t sensorCalVal[LS_NUM_SENSORS];      // an array of the calibrated values from the previous array
uint16_t sensorMaxVal[LS_NUM_SENSORS];      // the maximum sensor value from the line sensors
uint16_t sensorMinVal[LS_NUM_SENSORS];      // the minimum sensor value from the line sensors
uint8_t lineColor = 1;                      // white line of black background
int firstIndex = 0;                         // indicates you want to start a function that takes in an array at the array's first value
int calibrationValues = 100;                // the number of values from the line sensors to be calibrated
const int disArraySize = 5;                 // defines the number of values to average over when determining proper speed
int distanceArray[disArraySize] = {30, 30, 30, 30, 30};       // creates an empty array of size disArraySize that will hold the past x number of distances to an object
const int leftCutoff = 3100;                // if the linePos value is less than this and greater than zero, there is a line the the left of the center of the robot
const int rightCutoff = 3400;               // if the linePos value is greater than this, there is a line the the right of the center of the robot
int currentDistance;                        // establishing a global variable that will track the current distance to an object
const int closeCutoff = 20;                 // the maximum distance the robot will get to an object before adjusting its speed
const int targetDistance = 10;              // the maximum distance to an object before the robot will stop
const int normalSpeed = 13;                 // the normal speed at which the wheels will turn
const int fastSpeed = 19;                   // the faster speed at which the wheels will turn when adjusting its position a bit
const int calibrateSpeed = 20;              // the speed that will be used when calibrating the line following sensors

/* ENCODER VARIABLES */
float wheelDiameter = 2.7559055;            // the diameter of the wheel. Used for getting it to turn a certain angle
int cntPerRevolution = 360;                 // degrees in a 360 degree spin
float distAfterSpin = 1;                    // the distance the robot will move after turning, so it doesn't trigger another spin accidentally
float spinInches = 4.25;                    // the distance the robot will spin for a 90 degree angle, in inches, along a circular path
float spin180Inches = 8.7;                  // the distance the robot will spin for a 90 degree angle, in inches, along a circular path
int wheelSpeed = 55;

/* Serial1 MONITOR CONFIGURATION */
const int baudRate = 9600;                  // set baud rate for the Seriall Monitor

bool calibrated = false;                    // inital state of the calibrated boolean. this will make the robot calibrate once

void setup()
{
  /* Serial1 MONITOR */
  Serial1.begin(baudRate);                  // configure Serial1 Monitor with baudRate

  /* GRIPPER */
  gripper.attach(SRV_0);                    // this is pin 38, from RSLK_Pins.h

  /* WHEELS */
  rightWheel.begin(MOTOR_R_SLP_PIN,
                   MOTOR_R_DIR_PIN,
                   MOTOR_R_PWM_PIN);        // referred to RSLK_Pins.h for this initialization of the right wheel
  leftWheel.begin(MOTOR_L_SLP_PIN,
                  MOTOR_L_DIR_PIN,
                  MOTOR_L_PWM_PIN);         // referred to RSLK_Pins.h for this initialization of the left wheel

  stopMovement();                           // make sure the robot does not move before we start controlling it

  /* PINS */
  pinMode(IRpin, INPUT);                    // set IR pin as an input

  /* IR Receiver */
  initTinyIRReceiver();                     // initialize the infrared receiver

  /* PS2 */
  ps2x.config_gamepad(PS2_CLK,
                      PS2_CMD,
                      PS2_SEL,
                      PS2_DAT,
                      pressures,
                      rumble);              // configure the PS2 controller

  setupRSLK();                              // sets up the robot
  clearMinMax(sensorMinVal, sensorMaxVal);  // clears any stored min and max line sensor values

  delayMicroseconds(oneSec * MS);           // delay 1 second to ensure all configuration is complete

  closeGripperFull();                       // ensure the gripper is closed before the robot starts moving

}

/**************/
/*****LOOP*****/
/**************/
void loop()
{

  switch (STATE)                                            // check the current state...
  {
    case REMOTE:                                            // if we are in REMOTE state...
      ps2x.read_gamepad();                                  // read the PS2 gamepad
      if (decodeIR(&IRresults))                             // decodeIR() updates results and retunrs true if a new command is available
      {
        IRMode();                                           // calls IRMode() to perform the appropriate function based on which infrared remote button is pressed
      }
      delayMicroseconds(IRdelay * MS);                      // delays for 100 microseconds
      break;                                                // go back

    case PS2:                                               // if we are in PS2 state...
      PS2Mode();                                            // calls PS2Mode() to perform the appropriate function based on inputs on the controller
      ps2x.read_gamepad();                                  // read the PS2 gamepad for a potential state change
      if (ps2x.ButtonPressed(PSB_CIRCLE))                   // if the circle button on the PS2 controller has been pressed...
      {
        STATE = AUTO;                                       // toggle to auto state
        stopMovement();                                     // stop all movement for the moment
      }
      else if (ps2x.ButtonPressed(PSB_TRIANGLE))            // otherwise, if the triangle button on the PS2 controller has been pressed...
      {
        STATE = REMOTE;                                     // toggle to remote state
        stopMovement();                                     // stop all movement for the moment
      }
      break;                                                // go back

    case AUTO:                                              // if we are in AUTO state...
      lineFollow();                                         // calls autoMode() to perform the appropriate function based on inputs on the controller
      ps2x.read_gamepad();                                  // read the PS2 gamepad for a potential state change
      if (decodeIR(&IRresults))                             // decodeIR() updates results and retunrs true if a new command is available for a potential state change...
      {
        activateIR();                                       // calls activateIR() which will toggle state to REMOTE only if 0 was pressed
      }
      else if (ps2x.ButtonPressed(PSB_CROSS))               // otherwise, if the triangle button on the PS2 controller has been pressed...
      {
        STATE = PS2;                                        // toggle to PS2 state
        stopMovement();                                     // stop all movement for the moment
      }
      break;                                                // go back
  }
}

/*******************************************/
/*********AUTONOMOUS*****CONTROLS***********/
/*******************************************/

void lineFollow()                                                         // function to follow a line using the line sensors on the bottom of the robot
{
  int adjustedNormalSpeed = normalSpeed;                                  // the initial value of the normal speed is 13 
  int adjustedFastSpeed = fastSpeed;                                      // the initial value of the fast speed is 19
  
  readLineSensor(sensorVal);                                              // gets values from line sensor value array
  readCalLineSensor(sensorVal,                                            // gets values from calibrated line sensor value array
            sensorCalVal,
            sensorMinVal,
            sensorMaxVal,
            lineColor);

  uint32_t linePos = getLinePosition(sensorCalVal,lineColor);             // establishes linePos variable as the line's position, as told by the calibrated array
  Serial1.println(linePos);                                               // sends the current linePos to the serial monitor
  
  currentDistance = getDistanceSonar();                                   // sets currentDistance variable to the value from the sonar sensors

  setMotorDirection(BOTH_MOTORS, MOTOR_DIR_FORWARD);                      // wheels will spin forward

  enableMotor(BOTH_MOTORS);                                               // turn on both motors

  if (currentDistance < closeCutoff)                                      // if the current distance is within the distance where the robot's speed will be adjusted
  {
    adjustedNormalSpeed = adjustNormalSpeed(currentDistance);             // adjust the normal speed according to how close you are to an object
    adjustedFastSpeed = adjustFastSpeed(currentDistance);                 // adjust the fast speed according to how close you are to an object
  }

  if (linePos > 0 && linePos < leftCutoff)                                // if the line is detected on the left side of the robot
  {
    setMotorSpeed(LEFT_MOTOR,adjustedNormalSpeed);                        // make the left wheel go at the normal speed
    setMotorSpeed(RIGHT_MOTOR,adjustedFastSpeed);                         // make the right wheel go at the fast speed (will turn the robot to the left)
  } 
  else if (linePos > rightCutoff)                                         // if the line is detected on the right side of the robot
  {
    setMotorSpeed(LEFT_MOTOR,adjustedFastSpeed);                          // make the left wheel go at the fast speed
    setMotorSpeed(RIGHT_MOTOR,adjustedNormalSpeed);                       // make the right wheel go at the normal speed (will turn the robot to the right)
  }
  else if (linePos == 0)                                                  // if no line is detected
  {
    stopMovement();                                                       // stop the robot where it is
    spin90Left();                                                         // spin the robot 90 degrees to the left
    setMotorDirection(BOTH_MOTORS, MOTOR_DIR_FORWARD);                    // the wheels will spin forward
    goDistance(distAfterSpin);                                            // go a small distance, so the same line isn't accidentally read again
    readLineSensor(sensorVal);                                            // check again what the line sensor values are
    readCalLineSensor(sensorVal,                                          // check again what the calibrated line sensor values are
                      sensorCalVal,
                      sensorMinVal,
                      sensorMaxVal,
                      lineColor);
    linePos = getLinePosition(sensorCalVal,lineColor);                    // set the linePos variable to the new line position
    Serial1.println(linePos);                                             // write the new line position to the serial monitor
    
    if (linePos == 0)                                                     // if the line is still not detected
    {
      setMotorDirection(BOTH_MOTORS, MOTOR_DIR_BACKWARD);                 // robot will go backwards
      goDistance(distAfterSpin * 2);                                      // move twice the small distance, so the same line isn't accidentally read again
      spin180();                                                          // spin around 180 degrees
      readLineSensor(sensorVal);                                          // check again what the line sensor values are
      readCalLineSensor(sensorVal,                                        // check again what the calibrated line sensor values are
                      sensorCalVal,
                      sensorMinVal,
                      sensorMaxVal,
                      lineColor);
      linePos = getLinePosition(sensorCalVal,lineColor);                  // set the linePos variable to the new line position
      
      if (linePos == 0)                                                   // if the line is still not detected
      {
        setMotorDirection(BOTH_MOTORS, MOTOR_DIR_BACKWARD);               // both wheels will spin backward
        goDistance(distAfterSpin);                                        // go a small distance, so the same line isn't accidentally read again
        spin90Left();                                                     // spin 90 degrees to the left
        setMotorDirection(BOTH_MOTORS, MOTOR_DIR_BACKWARD);               // both motors will spin backward
        goDistance(distAfterSpin * 3);                                    // go a small distance, so the same line isn't accidentally read again
        spin180();                                                        // spin around 180 degrees
        openGripperFull();                                                // open the gripper all the way (drop whatever object the robot is holding)
        delayMicroseconds(halfSec*MS);                                    // delay for 0.5 seconds
      }
    }
  }
  else                                                                    // if the robot is still far away from an object
  {
    setMotorSpeed(LEFT_MOTOR,adjustedNormalSpeed);                        // left motor will spin at a normal speed
    setMotorSpeed(RIGHT_MOTOR,adjustedNormalSpeed);                       // right motor will spin at a normal speed
  }
}

int adjustNormalSpeed(int currentDistance)                                       // function to change the normal speed based on the distance to an object
{
  int error = currentDistance - targetDistance;                                  // sets the error as the difference between the current distance and the target distance
  int speed = constrain(map(error, 0, 14, 10, normalSpeed), 10, normalSpeed);    // adjusts the speed based on this difference
}

int adjustFastSpeed(int currentDistance)                                         // function to change the fast speed based on the distance to an object
{
  int error = currentDistance - targetDistance;                                  // sets the error as the difference between the current distance and the target distance
  int speed = constrain(map(error, 0, 14, 13, fastSpeed), 13, fastSpeed);        // adjusts the speed based on this difference
}

void calibrateLine() {                                                           // function to calibrate the line sensors (used in PS2 mode, before you switch to Auto mode)
  setMotorDirection(BOTH_MOTORS, MOTOR_DIR_FORWARD);                             // both wheels will spin forward
  enableMotor(BOTH_MOTORS);                                                      // turns both motors on
  setMotorSpeed(BOTH_MOTORS, calibrateSpeed);                                    // both wheels will turn at the calibrating speed, defined earlier
  Serial1.println("STARTING");                                                   // write to the serial monitor that the calibration has started
  for (int x = 0; x < calibrationValues; x++) {                                  // calibrate all of the line sensor values based on what they see in the robot's current position
    readLineSensor(sensorVal);
    setSensorMinMax(sensorVal, sensorMinVal, sensorMaxVal);
  }
  disableMotor(BOTH_MOTORS);                                                     // turns off both motors
  Serial1.println("DONE");                                                       // write to the serial monitor that the calibration has finished
}

/* Takes in the distance from both the left and right sonar sensors, averages them, adds them to an array of the last x values, and lastly returns the average of that array.
   Note that the new value is prepended to the array and then the last value is trimmed off to maintain the a constant array size. */
int getDistanceSonar() 
{
  long rightDistance;                                       // Distance from right sonar sensor
  long leftDistance;                                        // Distance from left sonar sensor
  long averageRightLeft;                                    // average between left and right distances from sonar
  for (int disIndex = firstIndex; disIndex < disArraySize; disIndex++)
  {
    long rightDistance = rightSonar.Distance();             // Distance from right sonar sensor
    long leftDistance = leftSonar.Distance();               // Distance from left sonar sensor
    int temparray[disArraySize];                            // Creates a temporary array to allow the old array to have the new value prepended
    distanceArray[0] = (rightDistance + leftDistance) / 2;  // Sets the old array's first term to the new value
    int rollingDistanceAvg = 0;                             // Creates a new variable that is used to calculate the average of the whole array
    for (int disindex = 0; disindex < disArraySize; disindex++)
    {
      temparray[disindex] = distanceArray[disindex];        // Creates a copy of the distanceArray and redfines it
    }
    for (int disindex = 0; disindex < disArraySize - 1; disindex++)
    {
      distanceArray[disindex + 1] = temparray[disindex];    // redefines terms 1 to the end to the previous 0 to end -1
    }
    for (int disindex = 0; disindex < disArraySize; disindex++)
    {
      rollingDistanceAvg += distanceArray[disindex];        // adds up all values in the list
    }
    rollingDistanceAvg /= disArraySize;                     // takes the added up values and divides it by the number of values in the list
    return rollingDistanceAvg;                              // the integer that the function will return
  }
}


void spin180()                                              // spins the robot 180 degrees
{
  rightWheel.directionBackward();                           // right wheel will spin backward
  leftWheel.directionForward();                             // left wheel will spin forward

  goDistance(spin180Inches);                                // spin halfway around, 180 degrees

  delayMicroseconds(halfSec * MS);                          // delay for 0.5 seconds
}

void spin90Left()                                           // spins the robot 90 degrees to the left
{
  rightWheel.directionForward();                            // right wheel will spin forward
  leftWheel.directionBackward();                            // left wheel will spin backward

  goDistance(spinInches);                                   // spin a quarter of the way around, 90 degrees counterclockwise

  delayMicroseconds(halfSec * MS);                          // delay for 0.5 seconds
}

void spin90Right()                                          // spins the robot 90 degrees to the right
{
  rightWheel.directionBackward();                           // right wheel will spin backward
  leftWheel.directionForward();                             // left wheel will spin forward

  goDistance(spinInches);                                   // spins a quarter of the way around, 90 degrees clockwise

  delayMicroseconds(halfSec * MS);                          // delay for 0.5 seconds
}

uint32_t countForDistance(float wheel_diam, uint16_t cnt_per_rev, float distance)           // integer for to calculate the distance the robot needs to spin, based on its physical qualities
{
  float temp = (wheel_diam * PI) / cnt_per_rev;                                             // calculations to determine how far the robot still needs to turn
  temp = distance / temp;
  return int(temp);
}

void goDistance(float goInches)                                                             // function to move a certain distance
{
  uint16_t rCount = 0;                                                                      // Total amount of encoder pulses received
  uint16_t lCount = 0;                                                                      // Total amount of encoder pulses received
  
  uint16_t ticks = countForDistance(wheelDiameter, cntPerRevolution, goInches);             // Amount of encoder pulses needed to achieve distance 

  resetLeftEncoderCnt();                                                                    // Set the right encoder pulses count back to zero
  resetRightEncoderCnt();                                                                   // Set the right encoder pulses count back to zero

  rightWheel.setRawSpeed(wheelSpeed);                                                       // set right motor speed
  leftWheel.setRawSpeed(wheelSpeed);                                                        // set left motor speed

  rightWheel.enableMotor();                                                                 // turns on right motor
  leftWheel.enableMotor();                                                                  // turns on left motor

  while (lCount < ticks - (wheelSpeed / 12.0) || rCount < ticks - (wheelSpeed / 12.0))      // Drive motor until it has received ticks pulses from both wheels. Subtraction is done because the robot keeps going a little even after we disable motors
  {
    ps2x.read_gamepad();                                                                    // checks for button presses on PS2 controller
    lCount = getEncoderLeftCnt();                                                           // get the left encoder pulses count, set to lCount variable
    rCount = getEncoderRightCnt();                                                          // get the right encoder pulses count, set to rCount variable

    if (lCount < rCount)                                                                    // if there are less left encoder pulses than right
    {
      stopMovement();                                                                       // stop all movement
      spinOne(LEFT, wheelSpeed);                                                            // turn slightly to the left
    }
    else if (lCount > rCount)                                                               // if there are more left encoder pulses than right
    {
      stopMovement();                                                                       // stop all movement
      spinOne(RIGHT, wheelSpeed);                                                           // turn slightly to the right
    }
    else                                                                                    // if there are an equal number of left and right encoder pulses
    {
      spinBoth(wheelSpeed, wheelSpeed);                                                     // spin both wheels at the preestablished speed
    }
  }

  /* Halt motors */
  rightWheel.disableMotor();                                                                // turn off right motor
  leftWheel.disableMotor();                                                                 // turn off left motor
}

/*******************************************/
/***********UNIVERSAL***FUNCTIONS***********/
/*******************************************/

void spinBoth(int rSpeed, int lSpeed)       // This function spins both wheels at specified speeds.   
{
  rightWheel.setRawSpeed(rSpeed);           // set the rightWheel's raw rotational speed to rValue
  leftWheel.setRawSpeed(lSpeed);            // set the leftWheel's raw rotational speed to lValue

  rightWheel.enableMotor();                 // start rotating rightWheel at the given speed
  leftWheel.enableMotor();                  // start rotating leftWheel at the given speed
}

void spinOne(int RL, int speed)             // spins the robot very slightly in either direction, based on what is given
{
  if (RL == RIGHT)                          // if you want to spin to the right
  {
    rightWheel.setRawSpeed(speed);          // set the right wheel to the given speed
    leftWheel.disableMotor();               // turn off the left motor
    rightWheel.enableMotor();               // turn on the right motor
  }
  else if (RL == LEFT)                      // if you want to spin to the left
  {
    leftWheel.setRawSpeed(speed);           // set the left wheel to the given speed
    rightWheel.disableMotor();              // turn off the right motor
    leftWheel.enableMotor();                // turn on the left motor
  }
}

void stopMovement()                         // function to stop all movement    
{
  rightWheel.disableMotor();                // disable the right motor
  leftWheel.disableMotor();                 // disable the left motor
}

void openGripperFull()                      // function to open the gripper all the way
{
  ps2x.read_gamepad();                      // checks if a button is pressed on the PS2 controller
  gripper.write(openGripperAngle);          // writes the maximum angle to the gripper servo
  gripperAngle = openGripperAngle;          // sets the gripperAngle variable to this new value
}

void closeGripperFull()                     // function to close the gripper all the way
{
  ps2x.read_gamepad();                      // check if a button is pressed on the PS2 controller
  gripper.write(closedGripperAngle);        // writes the minimum angle to the servo gripper
  gripperAngle = closedGripperAngle;        // sets the gripperAngle variable to this new value
}

void openGripper()                          // function to incrementally open the gripper
{
  ps2x.read_gamepad();                      // checks if a button is pressed on the PS2 controller
  if (gripperAngle - gripperChange <        // if the gripper would be opened beyond its maximum angle
      openGripperAngle)
  {
    gripper.write(openGripperAngle);        // set the gripper to its maximum angle
    gripperAngle = openGripperAngle;
  }
  else                                      // if the gripper would not be opened beyond its maximum angle
  {
    gripper.write(gripperAngle -            // increase the gripper's angle by a predetermined amount
                  gripperChange);           
    gripperAngle -= gripperChange;          // write the new angle to the gripperAngle variable
  }
}

void closeGripper()                         // function to incrementally close the gripper
{
  ps2x.read_gamepad();                      // checks if a button is pressed on the PS2 controller
  if (gripperAngle + gripperChange >        // if the gripper would be closed beyond its minimum angle
      closedGripperAngle)                    
  {
    gripper.write(closedGripperAngle);      // set the gripper to its minimum angle
    gripperAngle = closedGripperAngle;
  }
  else                                      // if the gripper would not be closed beyond its minimum angle
  {
    gripper.write(gripperAngle +            // decrease the gripper's angle by a predetermined amount
                  gripperChange);          
    gripperAngle += gripperChange;          // write the new angle to the gripperAngle variable
  }
}

/*******************************************/
/*****INFRARED******REMOTE*****CONTROLS*****/
/*******************************************/

void activateIR()
{
  switch (IRresults.command)                                // check for the new command from the infrared remote
  {
    case IR_FIVE:                                           // if (and only if) 5 is pressed...
      STATE = REMOTE;                                       // toggle state to REMOTE
      stopMovement();                                       // stop all movement for the moment
      break;                                                // go back

    default:                                                // default case...
      break;                                                // go back
  }
  delayMicroseconds(IRdelay);                               // delay for 0.1 seconds for the IR sensor
}

/*
   This function takes the appropriate action based on the IR code received
*/
void IRMode()
{
  switch (IRresults.command)                                // check for the new command from the infrared remote
  {
    case IR_UP:                                             // if up arrow is pressed...
      moveForward();                                        // move the robot forward
      break;                                                // go back

    case IR_DOWN:                                           // if down arrow is pressed...
      moveBackward();                                       // move the robot backward
      break;                                                // go back

    case IR_LEFT:                                           // if left arrow is pressed...
      spin(LEFT);                                           // spin the robot left
      break;                                                // go back

    case IR_RIGHT:                                          // if right arrow is pressed...
      spin(RIGHT);                                          // spin the robot right
      break;                                                // go back

    case IR_OK:                                             // if OK is pressed...
      stopMovement();                                       // stop the robot's movement
      break;                                                // go back

    case IR_ONE:                                            // if 1 is pressed...
      turn(LEFT);                                           // turn the robot left
      break;                                                // go back

    case IR_THREE:                                          // if 3 is pressed...
      turn(RIGHT);                                          // turn the robot right
      break;                                                // go back

    case IR_EIGHT:                                          // if 8 is pressed...
      STATE = AUTO;                                         // toggle to the automatic state
      stopMovement();                                       // stop all movement for the moment
      break;                                                // go back

    case IR_STAR:                                           // if * is pressed...
      openGripper();                                        // open the gripper
      break;                                                // go back

    case IR_HASH:                                           // if # is pressed...
      closeGripper();                                       // close the gripper
      break;                                                // go back

    case IR_ZERO:                                           // if 0 is pressed...
      STATE = PS2;                                          // toggle to PS2 state
      stopMovement();                                       // stop all movement for the moment
      break;                                                // go back

    default:                                                // default case...
      break;                                                // go back
  }
}

/****************************************/
/*****INFRARED******REMOTE*****ROBOT*****/
/****************************************/

/*
   This function spins the wheels forward for a specified duration in seconds.
   NOTE: this function treats the gripper as the front.
   The RSLK library uses directionBackward treating the gripper as the back.
*/
void moveForward()
{
  rightWheel.directionBackward();           // set rightWheel's direction to "backward"
  leftWheel.directionBackward();            // set leftWheel's direction to "backward"

  spinBoth(fullSpeed, fullSpeed);           // rotate both wheels at full speed
}

/*
   This function spins the wheels backward for a specified duration in seconds.
   NOTE: this function treats the gripper as the front.
   The RSLK library uses directionForward treating the gripper as the back.
*/
void moveBackward()
{
  rightWheel.directionForward();            // set rightWheel's direction to "forward"
  leftWheel.directionForward();             // set leftWheel's direction to "forward"

  spinBoth(fullSpeed, fullSpeed);           // rotate both wheels at full speed
}

/*
   This function spins the robot in a specified direction.
   NOTE: this function treats the gripper as the front.
   The reversed right/leftWheel and directionForward/Backward from the RSLK library cancel each other out.
*/
void spin(int RL)
{
  if (RL == RIGHT)                          // if we are told to spin right...
  {
    rightWheel.directionBackward();         // to spin right, we would make the right wheel rotate backward
    leftWheel.directionForward();           // make the left wheel rotate forward
  }
  else if (RL == LEFT)                      // if we are told to spin right...
  {
    rightWheel.directionForward();          // to spin left, we would make the right wheel rotate forward
    leftWheel.directionBackward();          // make the left wheel rotate backward
  }
  spinBoth(halfSpeed, halfSpeed);           // rotate both wheels at half speed
}

/*
   This function turns the robot in a specified direction.
   NOTE: this function treats the gripper as the front.
   The RSLK library function directionBackward() will make the robot move with the gripper in front.
*/
void turn(int RL)
{
  if (RL == RIGHT)                          // if we are told to turn right...
  {
    rightWheel.directionBackward();         // set the direction to "backward" (due to nature of the RSLK library)
    leftWheel.directionBackward();          // set the direction to "backward"
    spinBoth(fullSpeed, halfSpeed);         // we would turn the left wheel (treating the gripper as the front) faster to turn the robot right
  }
  else if (RL == LEFT)                      // if we are told to turn right...
  {
    rightWheel.directionBackward();         // set the direction to "backward"
    leftWheel.directionBackward();          // set the direction to "backward"
    spinBoth(halfSpeed, fullSpeed);         // turn the right wheel (treating the gripper as the front) faster to turn the robot left
  }
}

/**************************************/
/*****PS2******REMOTE*****CONTROLS*****/
/**************************************/

void PS2Mode()                                              // function to determine robot movement using PS2 buttons pressed and joystick positons
{
  ps2x.read_gamepad();                                      // read the PS2 gamepad for button presses and joystick positions

  if (ps2x.ButtonPressed(PSB_L1))                           // if the L1 button is pressed...
  {
    closeGripper();                                         // close the gripper
  }
  else if (ps2x.ButtonPressed(PSB_R1))                      // otherwise, if the R1 button is pressed...
  {
    openGripper();                                          // open the gripper
  }
  else if (ps2x.ButtonPressed(PSB_L2))                      // if the L2 button is pressed
  {
    calibrateLine();                                        // calibrates the line sensor values, which is used in autonomous mode
  }
  PS2Move();                                                // calls the PS2Move() function which handles all movement aspects of the robot according to joystick positions
}

void PS2Move()                                              // function to contrain values of PS2 joysticks to motor speeds
{
  int rightXvalue = constrain(                              // handles the X direction of the right joystick
                      map(                                  // change the raw values from the RIGHT joystick into mapped and constrained values for the motors
                        ps2x.Analog(PSS_RX),
                        minJoystick, maxJoystick,
                        -spinConversion, spinConversion),
                      -spinConversion, spinConversion);
  int rightYvalue = constrain(                              // handles the Y direction of the right joystick
                      map(ps2x.Analog(PSS_RY),              // change the raw values from the RIGHT joystick into mapped and constrained values for the motors
                          minJoystick, maxJoystick,
                          stopConversion, -stopConversion),
                      -stopConversion, stopConversion);
  int leftXvalue = constrain(                               // handles the X direction of the left joystick
                     map(ps2x.Analog(PSS_LX),               // Changes the raw values from the LEFT joystick into mapped and constrained values for the motor
                         minJoystick, maxJoystick,
                         -turnConversion, turnConversion),
                     -turnConversion, turnConversion);
  int leftYvalue = constrain(                               // handles the Y direction of the left joystick
                     map(ps2x.Analog(PSS_LY),               // Changes the raw values from the LEFT joystick into mapped and constrained values for the motor
                         minJoystick, maxJoystick,
                         forwardBackwardConversion, -forwardBackwardConversion),
                     -forwardBackwardConversion, forwardBackwardConversion);

  if (rightYvalue < -stopCutoff)                            // if the right joystick is significantly pressed downwards...
  {
    stopMovement();                                         // stop the robot's movement
  }
  else if (leftYvalue < -restingCutoff ||
           leftYvalue > restingCutoff)                      // otherwise, if the left joystick is pushed vertically (we use -5 and 5 to account for calibration error in the joysticks)
  {
    PS2TranslationMovement(leftXvalue, leftYvalue);         // translate the robot according to the left joystick's X and Y position
  }
  else                                                      // otherwise, we know that the left joystick is in its resting position at the center
  {
    stopMovement();                                         // therefore, stop the robot's movement
    if (rightXvalue > restingCutoff ||
        rightXvalue < -restingCutoff)                       // if the right joystick is pushed horizontally (we use -5 and 5 to account for calibration error in the joysticks)
    {
      PS2Spin(rightXvalue);                                 // spin the robot accoding to the right joystick's X position
    }
  }
}

void PS2TranslationMovement(int x, int y)                   // function to translate PS2 joystick positons to movement of the robot
{
  if (y < -restingCutoff)                                   // if the Y position of the left joystick is less than -5 (if it is pressed downwards), we will reverse...
  {
    rightWheel.directionForward();                          // set the right wheel's direction as forward
    leftWheel.directionForward();                           // set the left wheel's direction as forward
    spinBoth(-y, -y);                                       // rotate both wheels at a speed determined by the absolute value of the Y position of the left joystick
  }
  else if (y > restingCutoff)                               // otherwise, if the Y position of the left joystick is greater than 5 (if it is pressed upwards)...
  {
    rightWheel.directionBackward();                         // set the right wheel's direction as backward
    leftWheel.directionBackward();                          // set the left wheel's direction as backward
    if (x > restingCutoff)                                  // if the X position of the left joystick is greater than 5 (if it is pressed to the right), we have to turn right, too...
    {
      spinBoth(y + x, y);                                   // Rotate both wheels at a BASE speed determined by the absolute value of the Y position of the left joystick
    }
    else if (x < -restingCutoff)                            // otherwise, if the X position of the left joystick is less than -5 (if it is pressed to the left), we have to turn left...
    {
      spinBoth(y, y + (-x));                                // Rotate both wheels at a BASE speed determined by the absolute value of the Y position of the left joystick.
    }
    else                                                    // otherwise, we know we do not have to turn...
    {
      spinBoth(y, y);                                       // rotate both wheels at an equal speed determined by the absolute value of the Y position of the left joystick
    }
  }
}

void PS2Spin(int val)                                       // spins the robot while in PS2 mode
{
  if (val < -restingCutoff)                                 // if the X position of the right joystick is less than -5 (if it is pressed to the left), we have to spin left...
  {
    rightWheel.directionForward();                          // to spin left, we would make the right wheel rotate forward
    leftWheel.directionBackward();                          // make the left wheel rotate backward
  }

  else if (val > restingCutoff)                             // otherwise, if the X position of the right joystick is greater than 5 (if it is pressed to the right), we have to spin right...
  {
    leftWheel.directionForward();                           // to spin right, we would make the left wheel rotate forward
    rightWheel.directionBackward();                         // to spin right, we would make the right wheel rotate backward
  }

  spinBoth(abs(val), abs(val));                             // rotate both wheels at a speed determined by the absolute value of the X position of the right joystick
}
